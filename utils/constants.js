
/**
 * 假设这是服务器获取的数据
 */

const constants = [
  {
    "id": "id1",
    "name": "🎉路线推荐",
    "describe": "路线推荐",
    "category": [
      {
        "id": 1,
        "name": "所有景点路线",
        "num": 0,
        "routes_status":0,
        "text":"+"
      },
      {
        "id": 2,
        "name": "热门景点路线",
        "num": 0,
        "routes_status":0,
        "text":"+"

      },
      {
        "id": 3,
        "name": "人文历史路线",
        "num": 0,
        "routes_status":0,
        "text":"+"

      },{
        "id": 4,
        "name": "风景路线",
        "num": 0,
        "routes_status":0,
        "text":"+"
      }
    ],
  },
  {
    "id": "id2",
    "name": "🍔定制计划",
    "describe": "定制自己的游玩计划",
    "category": []
  }
]
// {
//   "category_id": 4,
//   "category_name": "category4",
//   "num": 0,
//   "routes_status":0,
//   "text":"+"
// },
// {
//   "category_id": 5,
//   "category_name": "category5",
//   "num": 0,
//   "routes_status":0,
//   "text":"+"
// },
// {
//   "category_id": 6,
//   "category_name": "category6",
//   "num": 0,
//   "routes_status":0,
//   "text":"+"
// },
// {
//   "category_id": 0,
//   "category_name": "category0",
//   "num": 0,
//   "routes_status":0,
//   "text":"+"
// },
// {
//   "category_id": 1,
//   "category_name": "category1",
//   "num": 0,
//   "routes_status":0,
//   "text":"+"
// },
// {
//   "category_id": 2,
//   "category_name": "category2",
//   "num": 0,
//   "routes_status":0,
//   "text":"+"
// }, {
//   "category_id": 3,
//   "category_name": "category3",
//   "num": 0,
//   "routes_status":0,
//   "text":"+"
// },
// {
//   "category_id": 7,
//   "category_name": "category7",
//   "num": 0,
//   "routes_status":0,
//   "text":"+"
// },
// {
//   "category_id": 8,
//   "category_name": "category8",
//   "num": 0,
//   "routes_status":0,
//   "text":"+"
// },
// {
//   "category_id": 12,
//   "category_name": "category12",
//   "num": 0,
//   "routes_status":0,
//   "text":"+"
// }
module.exports =  constants

