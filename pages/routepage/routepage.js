// 模拟服务器数据(假数据/记得在onload里请求服务器)
var amapFile = require('./map/utils/amap-wx.130.js');
const constants = require('../../utils/constants.js');
// 右侧每一类的 bar 的高度（固定）
const RIGHT_BAR_HEIGHT = 20;
// 右侧每个子类的高度（固定）
const RIGHT_ITEM_HEIGHT = 100;
// 左侧每个类的高度（固定）
const LEFT_ITEM_HEIGHT = 50
var httpUrl = "http://localhost:9090"
Page({
  data: {
    // 是否显示下面的购物车
    HZL_isCat: 0,

    // 购物车的商品
    HZL_my_cat: [],

    // 购物车的数量
    HZL_num: 0,

    // 左 => 右联动 右scroll-into-view 所需的id
    HZL_toView: null,

    // 右侧每类数据到顶部的距离（用来与 右 => 左 联动时监听右侧滚动到顶部的距离比较）
    HZL_eachRightItemToTop: [],
    HZL_leftToTop: 0,

    // 当前左侧选择的
    HZL_currentLeftSelect: null,

    // 数据
    constants: [],
    scenics: [],
    index2: 'fa',
    distance:0
  },
  onTabItemTap(item) {
    this.onLoad()
  },
  onLoad: function () {
    wx.request({
      url: httpUrl + '/scenic/list',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: (res) => {
        if (res.data.code == 20000) { //请求成功
          this.setData({
            scenics: res.data.data.list.data
          })
          // 更新数据
          this.setData({
            // 将请求的服务器数据赋值更新
            // constants: constants,
            'constants[1].category': this.data.scenics,
            HZL_currentLeftSelect: constants[0].id,
            HZL_eachRightItemToTop: this.HZL_getEachRightItemToTop()
          })
        }
      }
    })
    // console.log(constants,33233);
    let arr = [];
    //i是左边条数
    for (let i = 0; i < 2; i++) {
      arr.push(constants[i]);
    };

    this.setData({
      constants: arr,
    });
    // console.log(constants, 332);
    // 高度大小
    wx.getSystemInfo({
      success: (res) => {
        var HZL_height = res.windowHeight - 80
        var HZL_height1 = res.windowHeight - 110
        this.setData({
          HZL_height: HZL_height,
          HZL_height1: HZL_height1
        })
      }
    });

  },

  /**
   * 记录swiper滚动
   */
  HZL_swiperchange(e) {
    this.setData({
      HZL_swiper_ID: e.detail.current,
    })
  },


  /**
   * 点击分类栏
   */
  HZL_categories(e) {
    this.setData({
      HZL_swiper_ID: e.currentTarget.dataset.index
    })
  },


  /**
   * 获取每个右侧的 bar 到顶部的距离，
   * 用来做后面的计算。
   */
  HZL_getEachRightItemToTop() {
    var obj = {};
    var totop = 0;
    // 右侧第一类肯定是到顶部的距离为 0
    obj[constants[0].id] = totop
    // 循环来计算每个子类到顶部的高度
    for (let i = 1; i < (constants.length + 1); i++) {
      totop += (RIGHT_BAR_HEIGHT + constants[i - 1].category.length * RIGHT_ITEM_HEIGHT)
      // 这个的目的是 例如有两类，最后需要 0-1 1-2 2-3 的数据，所以需要一个不存在的 'last' 项，此项即为第一类加上第二类的高度。
      obj[constants[i] ? constants[i].id : 'last'] = totop
    }
    return obj
  },


  /**
   * 监听右侧的滚动事件与 HZL_eachRightItemToTop 的循环作对比
   * 从而判断当前可视区域为第几类，从而渲染左侧的对应类
   */
  right(e) {
    for (let i = 0; i < this.data.constants.length; i++) {
      let left = this.data.HZL_eachRightItemToTop[this.data.constants[i].id]
      let right = this.data.HZL_eachRightItemToTop[this.data.constants[i + 1] ? this.data.constants[i + 1].id : 'last']
      if (e.detail.scrollTop < right && e.detail.scrollTop >= left) {
        this.setData({
          HZL_currentLeftSelect: this.data.constants[i].id,
          HZL_leftToTop: LEFT_ITEM_HEIGHT * i
        })
      }
    }
  },


  /**
   * 左侧类的点击事件，
   * 点击时，右侧会滚动到对应分类
   */
  left(e) {
    this.setData({
      HZL_toView: e.target.id || e.target.dataset.id,
      HZL_currentLeftSelect: e.target.id || e.target.dataset.id
    })
  },





  /**
   * 是否显示下面的购物车
   */
  HZL_isCat: function (e) {
    var that = this;
    if (that.data.HZL_isCat == 0 && that.data.HZL_num > 0) {
      that.setData({
        HZL_isCat: 1
      })
    } else if (that.data.HZL_isCat == 1 && that.data.HZL_num > 0) {
      that.setData({
        HZL_isCat: 0
      })
    }
  },


  /**
   * 黑背景模态墙点外侧关闭
   */
  HZL_isCat_close: function (e) {
    this.setData({
      HZL_isCat: 0
    })
  },


  /**
   * 清空购物车
   */
  HZL_zero: function (e) {
    for (var i = 0; i < this.data.constants.length; i++) {

      for (var j = 0; j < this.data.constants[i].category.length; j++) {
        this.data.constants[i].category[j].num = 0
        this.data.constants[i].category[j].routes_status = 0;
      }
    }
    this.setData({
      HZL_isCat: 0,
      HZL_num: 0,
      HZL_my_cat: [],
      index2: "fa",
      constants: this.data.constants,
    })
  },

  clear() {
    this.setData({
      index2: 'fa',
      HZL_my_cat: [],
      HZL_num: 0
    })
  },
  /*
      点击view，展示view
   */
  showView(e) {
    var parentIndex = e.currentTarget.dataset.parentindex;
    var index = e.currentTarget.dataset.index;
    console.log(e.currentTarget.dataset.index);
    wx.navigateTo({
      url: 'informations',
      success: function (res) {
        // 通过eventChannel向被打开页面传送数据====================点击请求图片列表一起传=====================================
        res.eventChannel.emit('acceptDataFromOpenerPage', {
          routes_data: [parentIndex, index]
        })
      }
    })

    //这里有个根据id请求后端，展示路线，景点信息
  },
  /*
    所有景点路线的添加按钮
  */
  routes_all_btn(e) {
    //index 是选择哪条景点路线
    var index = e.currentTarget.dataset.index;
    var parentIndex = 1;
    this.data.constants[0].category[index].routes_status = 1;
    var alllist = this.data.constants[1].category;
    var hot_list = []
    switch (index) {
      case 0:
        hot_list = [10, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        break;
      case 1:
        hot_list = [1, 2, 6, 7, 9];
        break;
      case 2:
        hot_list = [10, 3, 4, 6, 7, 9];
        break;
      case 3:
        hot_list = [10, 5];
        break;
    }


    //根据点击的index，给列表赋值
    var add_list = hot_list
    for (var j = 0; j < add_list.length; j++) {
      for (var i = 0; i < alllist.length; i++) {
        if (add_list[j] == alllist[i].id) {
          var HZL_my_cat = this.HZL_my_jia(parentIndex, alllist[i]);
          break;
        }
      }
    }
    this.setData({
      index2: index,
      HZL_num: HZL_my_cat.length,
      HZL_my_cat: HZL_my_cat,
      constants: this.data.constants,
    })
  },


  /**
   * 增加数量按钮
   */
  HZL_jia(e) {

    var item = e.currentTarget.dataset.category;
    var parentIndex = e.currentTarget.dataset.parentindex;
    var HZL_my_cat = this.HZL_my_jia(parentIndex, item)
    this.setData({
      HZL_num: this.data.HZL_num,
      HZL_my_cat: HZL_my_cat,
      constants: this.data.constants,
    })
  },
  /**
   * 封装加的方法
   */
  HZL_my_jia: function (parentIndex, item) {

    this.data.HZL_num++;
    var num = -1;
    var templist = this.data.constants[parentIndex].category;
    for (var i = 0; i < templist.length; i++) {
      if (item.id == templist[i].id) {
        num = this.data.constants[parentIndex].category[i].num = 1;
        break;
      }
    }
    var index = item;
    var parentIndex = parentIndex;
    var name = item.name;
    item.routes_status = 1;
    var status = item.routes_status;
    //弄一个元素判断会不会是重复的
    var mark = 'a' + index.id + 'b' + parentIndex + 'c' + '0' + 'd' + '0'
    var obj = {
      num: num,
      name: name,
      mark: mark,
      index: index,
      parentIndex: parentIndex,
      routes_status: status
    };
    var HZL_my_cat = this.data.HZL_my_cat;
    HZL_my_cat.push(obj)

    var arr = [];
    //去掉重复的
    for (var i = 0; i < HZL_my_cat.length; i++) {
      if (obj.mark == HZL_my_cat[i].mark) {
        HZL_my_cat.splice(i, 1, obj)
      }
      if (arr.indexOf(HZL_my_cat[i]) == -1) {
        arr.push(HZL_my_cat[i]);
      }
    }
    return arr
  },

  /**
   * 减少数量按钮
   */
  HZL_jian: function (e) {
    var item = e.currentTarget.dataset.category;
    var parentIndex = e.currentTarget.dataset.parentindex;
    var HZL_my_cat = this.HZL_my_jian(parentIndex, item)
    if (this.data.HZL_num == 0) {
      this.data.HZL_isCat = 0;
    } else {
      this.data.HZL_isCat = 1;
    }
    this.setData({
      HZL_num: this.data.HZL_num,
      HZL_my_cat: HZL_my_cat,
      constants: this.data.constants,
    })
  },


  /**
   * 下面购物车减少按钮
   */
  HZL_jian1: function (e) {

    var item = e.currentTarget.dataset.category.index;
    var parentIndex = e.currentTarget.dataset.parentindex;
    var HZL_my_cat = this.HZL_my_jian(parentIndex, item);
    if (this.data.HZL_num == 0) {
      this.data.HZL_isCat = 0;
    } else {
      this.data.HZL_isCat = 1;
    }
    this.setData({
      HZL_num: this.data.HZL_num,
      HZL_my_cat: HZL_my_cat,
      constants: this.data.constants,
      HZL_isCat: this.data.HZL_isCat
    })
  },

  /**
   * 封装减的方法
   */
  HZL_my_jian: function (parentIndex, item) {
    this.data.HZL_num--;
    var num = -1;
    var templist = this.data.constants[parentIndex].category;
    for (var i = 0; i < templist.length; i++) {
      if (item.id == templist[i].id) {
        num = --this.data.constants[parentIndex].category[i].num;
        break;
      }
    }
    var index = item;
    var parentIndex = parentIndex;
    var name = item.name;

    item.routes_status = 0;
    var status = item.routes_status;
    //弄一个元素判断会不会是重复的
    var mark = 'a' + index.id + 'b' + parentIndex + 'c' + '0' + 'd' + '0'
    var obj = {
      num: num,
      name: name,
      mark: mark,
      index: index,
      parentIndex: parentIndex,
      routes_status: status
    };
    var HZL_my_cat = this.data.HZL_my_cat;
    HZL_my_cat.push(obj)
    var arr = [];
    //去掉重复的
    for (var i = 0; i < HZL_my_cat.length; i++) {
      if (obj.mark == HZL_my_cat[i].mark) {
        HZL_my_cat.splice(i, 1, obj)
      }
    }


    var arr1 = [];
    //当数量大于1的时候加
    for (var i = 0; i < HZL_my_cat.length; i++) {
      if (arr1.indexOf(HZL_my_cat[i]) == -1) {
        arr1.push(HZL_my_cat[i]);
        if (HZL_my_cat[i].num > 0) {
          arr.push(arr1[i])
        }
      }
    }

    return arr
  },
  
  /**
   * 去结算按钮
   * 具体功能自己写
   */
  go: function () {
    var ss = this.data.HZL_my_cat
    if(ss.length!=0){
     //获取当前位置
     //获取当前的地理位置、速度
     wx.getLocation({
      type: 'wgs84', // 默认为 wgs84 返回 gps 坐标，gcj02 返回可用于 wx.openLocation 的坐标
      success: function (res) {
        //赋值经纬度
          wx.setStorageSync('myLandl', res.longitude+","+res.latitude)
      }
    })
    var dislist = [];
    dislist.push(wx.getStorageSync('myLandl'))
    var idList = ["888"];
    
    for(var i=0;i<ss.length;i++){
      dislist.push(ss[i].index.landl)
      idList.push(ss[i].index.id)
    }    
    var chuan = {"disList":dislist,"idList":idList}
    // 根据清单表请求后台，通过算法生成路由路线
    wx.request({
      url: httpUrl + '/scenic/calculateRoutes',
      data:JSON.stringify(chuan),
      method:'POST',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: (res) => {
        if (res.data.code == 20000) { //请求成功
          var datalist = res.data.data.list
          // wx.setStorageSync('finshroute', res.data.data.list)
          wx.navigateTo({
            url: 'map/routemap',
            success: function (res) {
              console.log("传递到地图页面的数据")
              console.log(datalist)
              // 通过eventChannel向被打开页面传送数据====================点击请求图片列表一起传=====================================
              res.eventChannel.emit('acceptDataFromOpenerPage', {
                   routes: datalist
              })
            }
          })
        }
      }
    })
    }else{
      wx.showToast({
        title: '请选择景点',
        icon: 'error',
        duration: 2000
       })
    }
  },
})