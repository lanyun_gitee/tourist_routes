// pages/routepage/informations.js


var httpUrl = "http://localhost:9090"
Page({
    /**
     * 页面的初始数据
     */
    data: {
        // 判断传进来的id
        showroutes:false,
        showroutes11:1,
        scenic:"",
        i_urls:[]
    },
    setshowview(e){
        console.log(1)
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (option) {
      
        let that = this
        const eventChannel = this.getOpenerEventChannel()
        // 监听acceptDataFromOpenerPage事件，获取上一页面通过eventChannel传送到当前页面的数据
        eventChannel.on('acceptDataFromOpenerPage', (routes_data)=> {
            if(routes_data.routes_data[0]==0){
                that.setData({
                    showroutes:true,
                  })               
            }
            that.setData({
                scenic:routes_data.routes_data[1]
            })
            wx.request({
                url: httpUrl + '/images/img/'+routes_data.routes_data[1].imageId,
                header: {
                  'content-type': 'application/json' // 默认值
                },
                success: (res) => {
                  if (res.data.code == 20000) { //请求成功
                    this.setData({
                      i_urls: res.data.data.urls
                    })
                  }
                }
              })
            
        })

        
        
    },
    
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})