var amapFile = require('./utils/amap-wx.130.js');
// var httpUrl = "http://192.168.0.118:9090"
var httpUrl = "http://localhost:9090"

Page({
  data: {
    routesurl: [],
    // {name:"南桥",coordinates:'103.615948,30.996316'},
    // {name:"宝瓶口",coordinates:"103.612781,30.998543"},
    // {name:"玉垒阁",coordinates:"103.614333,31.001895"} 
    steps: {},
    markers: [],
    distance: '',
    cost: '',
    polyline: [],
    secend:"",
  },
  onLoad: function() {
    
    let that = this
      const eventChannel = this.getOpenerEventChannel()
      // 监听acceptDataFromOpenerPage事件，获取上一页面通过eventChannel传送到当前页面的数据
      eventChannel.on('acceptDataFromOpenerPage', (routes)=> {
        var tmp = [];
        for(var i=routes.routes.length-1;i>=0;i--){
          tmp.push(routes.routes[i])
        }
        that.setData({
            routesurl:tmp
        })
      })
      this.initdata()
      this.setDynamicLocation()
  },
  initdata(){
    var initMarkers = [{
      iconPath: "./img/mapicon_navi_e.png",
      id: 1,
      latitude:"",
      longitude:"",
      width: 23,
      height: 33
    },{
      iconPath: "./img/mapicon_navi_s.png",
      id: 0,
      latitude:"",
      longitude:"",
      width: 24,
      height: 34
    }]
    wx.getLocation({
      type: 'wgs84', // 默认为 wgs84 返回 gps 坐标，gcj02 返回可用于 wx.openLocation 的坐标
      success: function (res) {
        //赋值经纬度
          wx.setStorageSync('latitude', res.latitude),
          wx.setStorageSync('longitude', res.longitude)
      }
    })
    var cor = this.data.routesurl[0].landl.split(",");
    var name = this.data.routesurl[0].name;
    initMarkers[0].longitude = cor[0];
    initMarkers[0].latitude = cor[1];
    initMarkers[1].longitude = wx.getStorageSync('longitude')
    initMarkers[1].latitude = wx.getStorageSync('latitude')
    this.setData({
      markers:initMarkers,
      secend:name
    })
  },
  // 改变目的地的坐标，并且重新规划线路
  setDynamicLocation(){
    var that = this;
    var key = "cae83d7faee406572cfab072f1c01b1d";
    var myAmapFun = new amapFile.AMapWX({key: key});
    // 起点
    var origin1 = this.data.markers[1].longitude+","+ this.data.markers[1].latitude
    // 目的地
    var des = this.data.markers[0].longitude+","+ this.data.markers[0].latitude
    myAmapFun.getWalkingRoute({
      origin: origin1,
      destination: des,
      success: function(data){
        var points = [];
        if(data.paths && data.paths[0] && data.paths[0].steps){
          var steps = data.paths[0].steps;
          that.setData({
            steps: data.paths[0].steps
          });
          for(var i = 0; i < steps.length; i++){
            var poLen = steps[i].polyline.split(';');
            for(var j = 0;j < poLen.length; j++){
              points.push({
                longitude: parseFloat(poLen[j].split(',')[0]),
                latitude: parseFloat(poLen[j].split(',')[1])
              })
            } 
          }
        }
        that.setData({
          polyline: [{
            points: points,
            color: "#0091ff",
            width: 6
          }]
        });
        if(data.paths[0] && data.paths[0].distance){
          that.setData({
            distance: data.paths[0].distance + '米'
          });
        }
        if(data.paths[0] && data.paths[0].duration){
          that.setData({
            cost: parseInt(data.paths[0].duration/60) + '分钟'
          });
        }
          
      },
      fail: function(info){

      }
    })
  },
  changeL(e){
    var initMarkers = [{
      iconPath: "./img/mapicon_navi_e.png",
      id: 1,
      latitude:"",
      longitude:"",
      width: 23,
      height: 33
    },{
      iconPath: "./img/mapicon_navi_s.png",
      id: 0,
      latitude:"30.99421",
      longitude:"103.614429",
      width: 24,
      height: 34
    }]
     //获取当前位置
     //获取当前的地理位置、速度
     wx.getLocation({
      type: 'wgs84', // 默认为 wgs84 返回 gps 坐标，gcj02 返回可用于 wx.openLocation 的坐标
      success: function (res) {
        //赋值经纬度
          wx.setStorageSync('latitude', res.latitude),
          wx.setStorageSync('longitude', res.longitude)
      }
    })
    var decCoordinates = e.currentTarget.dataset.secitem.landl;
    var name = e.currentTarget.dataset.secitem.name;
    var cor = decCoordinates.split(",");
    initMarkers[0].longitude = cor[0];
    initMarkers[0].latitude = cor[1];
    initMarkers[1].longitude = wx.getStorageSync('longitude')
    initMarkers[1].latitude = wx.getStorageSync('latitude')
    this.setData({
      markers:initMarkers,
      secend:name
    })
   this.setDynamicLocation()
  }

})