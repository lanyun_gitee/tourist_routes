//login.js
//获取应用实例
var app = getApp();
Page({
  data: {
    remind: '加载中',
    angle: 0,
    year: 2022,
    userInfo: {}
  },
  goToIndex: function () {
    //发起登录请求
    wx.login({
      success(res) {
        console.log(res)
        var userInfo =wx.getStorageSync("userInfo");
        console.log("2222"+userInfo);
        var code = res.code
        console.log(code)
        wx.request({
          url: 'http://localhost:9090/user/login',
          // data:code,
          data: {
            code:code,
            userInfo:userInfo
          },
          method: 'POST',
          success: (res) => {
            console.log(".........")
            wx.switchTab({
              url: '/pages/index/index'
            })
          },
        })
      }
    })
  },
  onLoad: function () {
    var that = this
    let userInfo = wx.getStorageSync('userInfo')
    that.setData({
      userInfo: userInfo
    })
  },
  onShow: function () {
    
  },
  onReady: function () {
    var _this = this;
    setTimeout(function () {
      _this.setData({
        remind: ''
      });
    }, 1000);
    wx.onAccelerometerChange(function (res) {
      var angle = -(res.x * 30).toFixed(1);
      if (angle > 14) { angle = 14; }
      else if (angle < -14) { angle = -14; }
      if (_this.data.angle !== angle) {
        _this.setData({
          angle: angle
        });
      }
    });
  },
});