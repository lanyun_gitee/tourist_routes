// index.js
// 获取应用实例
const app = getApp()

Page({
  data: {
    imgUrls: [
      'https://gulidemo-1756.oss-cn-chengdu.aliyuncs.com/titles/1.png',
      'https://gulidemo-1756.oss-cn-chengdu.aliyuncs.com/titles/2.png',
      'https://gulidemo-1756.oss-cn-chengdu.aliyuncs.com/titles/3.png' 
    ],
    indicatorDots: true,
    autoplay: true,
    interval: 3000,
    duration: 1000
  }
})